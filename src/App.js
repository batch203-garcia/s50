import { Container } from 'react-bootstrap';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Courses from './pages/Courses';
import Home from './pages/Home';

function App() {
  return (
    <div className="App">
      <>
      <AppNavbar/>

      <Container fluid>
      <Home/>
      <Courses/>
      </Container>

      

      </>
    </div>
  );
}

export default App;
