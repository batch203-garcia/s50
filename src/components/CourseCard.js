import { useState } from "react";
import {Button, Card} from "react-bootstrap"

export default function CoursesCard({courseProp}){
    const {name, description, price} = courseProp;

    const [count, setCount] = useState(0);
    console.log(count = 5);
    console.log(useState(10));

    return(
                <Card className="my-3">
                    <Card.Body>
                        <Card.Title>
                            {name}
                        </Card.Title>
                        <Card.Subtitle>
                            Description:
                        </Card.Subtitle>
                        <Card.Text>
                        {description}
                        </Card.Text>
                        <Card.Subtitle>
                            Price:
                        </Card.Subtitle>
                        <Card.Text>
                        Php {price}
                        </Card.Text>
                        <Button variant="primary" >Enroll</Button>
                    </Card.Body>
                </Card>
    )
}